package com.doan.codewarsjavakatas.katas.kata0003;

// https://www.codewars.com/kata/54ba84be607a92aa900000f1

public class isogram {

    public static boolean isIsogram(String str) {
        str = str.toLowerCase();
        for (int i = 0; i < str.length(); i++) {
            String substr = str.substring(i, i + 1);
            for (int j = i + 1; j < str.length(); j++) {
                if (substr.equals(str.substring(j, j + 1))) {
                    return false;
                }
            }
        }
        return true;
    }

}
