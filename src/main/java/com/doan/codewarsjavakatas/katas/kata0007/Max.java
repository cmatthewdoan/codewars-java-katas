package com.doan.codewarsjavakatas.katas.kata0007;

// https://www.codewars.com/kata/54521e9ec8e60bc4de000d6c

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Max {

    public static int sequence(int[] arr) {
        if (isArrayNegativeOrEmpty(arr)) {
            return 0;
        }
        int max = Arrays.stream(arr).max().getAsInt();
        for (int subArraySize = 2; subArraySize < arr.length; subArraySize++) {
            List<int[]> subArrays = splitIntoContiguousSubArrays(arr, subArraySize);
            for(int[] subArray : subArrays) {
                max = Integer.max(max, Arrays.stream(subArray).sum());
            }
        }
        return max;
    }

    public static boolean isArrayNegativeOrEmpty(int[] numbers) {
        return Arrays.stream(numbers).boxed().filter(i -> i >= 0).collect(Collectors.toList()).isEmpty();
    }

    public static List<int[]> splitIntoContiguousSubArrays(int[] arrayToSplit, int subArraySize) {
        int numberOfSubArrays = Integer.max(0, arrayToSplit.length - (subArraySize - 1));
        List<int[]> subArrays = new ArrayList<>();
        for (int startIndex = 0; startIndex < numberOfSubArrays; startIndex++) {
            int endIndex = startIndex + subArraySize;
            int[] subArray = Arrays.copyOfRange(arrayToSplit, startIndex, endIndex + 1);
            subArrays.add(subArray);
        }
        return subArrays;
    }

}
