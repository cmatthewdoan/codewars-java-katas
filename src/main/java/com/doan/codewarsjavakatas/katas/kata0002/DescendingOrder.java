package com.doan.codewarsjavakatas.katas.kata0002;

// https://www.codewars.com/kata/5467e4d82edf8bbf40000155

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DescendingOrder {
    public static int sortDesc(final int num) {
        List<String> digits = digitize(num);
        Collections.sort(digits, Collections.reverseOrder());

        String sortedNumber = "";
        for (String digit : digits) {
            sortedNumber += digit;
        }
        return Integer.parseInt(sortedNumber);
    }

    public static List<String> digitize(final int num) {
        String number = Integer.toString(num);
        List<String> digits = new ArrayList<>(number.length());
        for (int i = 0; i < number.length(); i++) {
            digits.add(number.substring(i, i + 1));
        }
        return digits;
    }
}