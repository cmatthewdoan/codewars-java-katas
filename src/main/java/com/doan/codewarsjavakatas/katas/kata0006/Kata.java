package com.doan.codewarsjavakatas.katas.kata0006;

// https://www.codewars.com/kata/525f50e3b73515a6db000b83

public class Kata {

    public static String createPhoneNumber(int[] digits) {
        return "(" + digits[0] + digits[1] + digits[2] + ") " + digits[3] + digits[4] + digits[5] + "-" + digits[6] + digits[7] + digits[8] + digits[9];
    }

    public static String createPhoneNumber2(int[] numbers) {
        return String.format("(%d%d%d) %d%d%d-%d%d%d%d", java.util.stream.IntStream.of(numbers).boxed().toArray());
    }

    public static String createPhoneNumber3(int[] numbers) {
        String phoneNumber = new String("(xxx) xxx-xxxx");
        for (int i : numbers) {
            phoneNumber = phoneNumber.replaceFirst("x", Integer.toString(i));
        }
        return phoneNumber;
    }
}
