package com.doan.codewarsjavakatas.katas.kata0009;

// https://www.codewars.com/kata/51b62bf6a9c58071c600001b

// REF: https://blog.prepscholar.com/roman-numerals-converter
/* As you probably noticed above, Roman numerals only go up to M (1,000).
 * According to the rules of addition and subtraction,
 * this means that the biggest number we can form in Roman numerals is MMMCMXCIX, or 3,999. */

import java.util.HashMap;
import java.util.Map;

public class Conversion {

    private static final Map<Integer, String> CONVERSION_MAP = new HashMap<>() {
        {
            put(1, "I");
            put(5, "V");
            put(10, "X");
            put(50, "L");
            put(100, "C");
            put(500, "D");
            put(1000, "M");
        }
    };

    public String solution(int n) {
        // STEPS
        // digitize the number into a list of expanded values (see kata0008)
        // convert each expanded value into a roman numeral representation (this is the bulk of the problem)
        // concatenate the roman numeral representations together
        // return the result

        String result = CONVERSION_MAP.get(n);
        return (result != null) ? result : "";
    }
}
