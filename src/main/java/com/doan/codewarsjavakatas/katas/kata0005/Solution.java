package com.doan.codewarsjavakatas.katas.kata0005;

// https://www.codewars.com/kata/514b92a657cdc65150000006

import java.util.stream.IntStream;

class Solution {

    public int solution(int number) {
        int sum = 0;
        for (int i = 0; i < number; i++) {
            if (i % 3 == 0 || i % 5 == 0) {
                sum += i;
            }
        }
        return sum;
    }

    public int solutionAlternate(int number) {
        return IntStream.range(0, number)
                .filter(n -> (n % 3 == 0) || (n % 5 == 0))
                .sum();
    }

}