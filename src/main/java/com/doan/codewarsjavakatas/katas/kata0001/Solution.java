package com.doan.codewarsjavakatas.katas.kata0001;

// https://www.codewars.com/kata/5266876b8f4bf2da9b000362

class Solution {

    public static String whoLikesIt(String... names) {
        String[] nameArray = new String[names.length];
        int index = 0;
        for (String name : names) {
            nameArray[index] = name;
            index++;
        }

        if (nameArray.length < 1) {
            return "no one likes this";
        } else if (nameArray.length == 1) {
            return nameArray[0] + " likes this";
        } else if (nameArray.length == 2) {
            return nameArray[0] + " and " + nameArray[1] + " like this";
        } else if (nameArray.length == 3) {
            return nameArray[0] + ", " + nameArray[1] + " and " + nameArray[2] + " like this";
        } else {
            int xOthers = nameArray.length - 2;
            return nameArray[0] + ", " + nameArray[1] + " and " + xOthers + " others like this";
        }
    }
}