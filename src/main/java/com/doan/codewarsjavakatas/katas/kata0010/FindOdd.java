package com.doan.codewarsjavakatas.katas.kata0010;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

// https://www.codewars.com/kata/54da5a58ea159efa38000836/java

public class FindOdd {

    public static int findIt(int[] a) {
        Map<Integer, Integer> occurrencesMap = Arrays.stream(a).boxed().collect(Collectors.toMap(val -> val, val -> 1, (a1, a2) -> a1 + a2));
        List<Integer> odds = occurrencesMap.keySet().stream().filter(o -> occurrencesMap.get(o) % 2 == 1).collect(Collectors.toList());
        return (odds.size() == 1) ? odds.get(0) : 0;
    }

    // Alternate (someone else's genius solution)
    public static int findIt2(int[] A) {
        int odd = 0;
        for (int item : A) {
            odd = odd ^ item;// XOR will cancel out everytime you XOR with the same number so 1^1=0 but 1^1^1=1 so every pair should cancel out leaving the odd number out
        }

        return odd;
    }

}