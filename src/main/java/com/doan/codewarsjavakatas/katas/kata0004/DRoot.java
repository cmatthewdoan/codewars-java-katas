package com.doan.codewarsjavakatas.katas.kata0004;

// https://www.codewars.com/kata/541c8630095125aba6000c00

public class DRoot {

    public static int digital_root(int n) {
        int droot = sumOfDigits(n);
        while (String.valueOf(droot).length() > 1) {
            droot = sumOfDigits(droot);
        }
        return droot;
    }

    public static int sumOfDigits(int n) {
        String[] digits = String.valueOf(n).split("");
        int sum = 0;
        for (String digit : digits) {
            sum += Integer.parseInt(digit);
        }
        return sum;
    }

}
