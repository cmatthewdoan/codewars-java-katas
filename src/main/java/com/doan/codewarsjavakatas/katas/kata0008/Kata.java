package com.doan.codewarsjavakatas.katas.kata0008;

// https://www.codewars.com/kata/5842df8ccbd22792a4000245

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Kata {

    public static String expandedForm(int num) {
        String[] digits = String.valueOf(num).split("");
        List<String> expandedDigits = new ArrayList<>(digits.length);

        for (int i = 0; i < digits.length; i++) {
            int power = digits.length - 1 - i;
            int expandedValue = (int) (Integer.parseInt(digits[i]) * Math.pow(10.0, power));
            if (expandedValue > 0) {
                expandedDigits.add(Integer.toString(expandedValue));
            }
        }

        String solution = "";
        Iterator<String> expandedDigitsIterator = expandedDigits.listIterator();
        while(expandedDigitsIterator.hasNext()) {
          solution += expandedDigitsIterator.next();
          if(expandedDigitsIterator.hasNext()) {
              solution += " + ";
          }
        }

        if(solution.isBlank()) {
            solution = "0";
        }

        return solution;
    }
}
