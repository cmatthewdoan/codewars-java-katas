package com.doan.codewarsjavakatas;

public class CodewarsJavaKatasApplication {

    public static void main(String[] args) {
        System.out.println("This does nothing. The kata solutions are verified through tests, but there's no actual program here.\nSee: https://www.codewars.com");
    }

}
