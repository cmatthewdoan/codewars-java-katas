package com.doan.codewarsjavakatas.katas.kata0005;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SolutionTest {

    @Test
    public void test() {
        assertEquals(23, new Solution().solution(10));
    }

    @Test
    public void test_alternate() {
        assertEquals(23, new Solution().solutionAlternate(10));
    }
}
