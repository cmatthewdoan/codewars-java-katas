package com.doan.codewarsjavakatas.katas.kata0002;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DescendingOrderTest {

    @Test
    public void test_01() {
        assertEquals(0, DescendingOrder.sortDesc(0));
    }

    @Test
    public void test_02() {
        assertEquals(51, DescendingOrder.sortDesc(15));
    }

    @Test
    public void test_03() {
        assertEquals(987654321, DescendingOrder.sortDesc(123456789));
    }

    @Test
    public void test_04() {
        assertEquals(987654321, DescendingOrder.sortDesc(798651324));
    }

    @Test
    public void test_05() {
        assertEquals(876543000, DescendingOrder.sortDesc(708650304));
    }

    @Test
    public void test_06() {
        assertEquals(54421, DescendingOrder.sortDesc(42145));
    }

    @Test
    public void test_07() {
        assertEquals(654321, DescendingOrder.sortDesc(145263));
    }

    @Test
    public void test_08() {
        assertEquals(987654321, DescendingOrder.sortDesc(123456789));
    }

    @Test
    public void digitize_0_should_return_0() {
        // GIVEN
        int num = 0;
        // WHEN
        List<String> actualDigits = DescendingOrder.digitize(num);
        // THEN
        List<String> expectedDigits = Arrays.asList(new String[]{"0"});
        Assert.assertArrayEquals(expectedDigits.toArray(), actualDigits.toArray());
    }

    @Test
    public void digitize_MAX_VALUE_should_return_2_1_4_7_4_8_3_6_4_7() {
        // GIVEN
        int num = Integer.MAX_VALUE;
        // WHEN
        List<String> actualDigits = DescendingOrder.digitize(num);
        // THEN
        List<String> expectedDigits = Arrays.asList(new String[]{"2", "1", "4", "7", "4", "8", "3", "6", "4", "7"});
        Assert.assertArrayEquals(expectedDigits.toArray(), actualDigits.toArray());
    }
}
