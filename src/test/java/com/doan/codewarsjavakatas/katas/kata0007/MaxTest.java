package com.doan.codewarsjavakatas.katas.kata0007;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MaxTest {

    @Test
    public void testExampleArray() throws Exception {
        assertEquals("Example array should have a max of 6", 6, Max.sequence(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4}));
    }

    @Test
    public void testSingleElementArray() throws Exception {
        assertEquals("Array should have a max of 5", 5, Max.sequence(new int[]{5}));
    }

    @Test
    public void testEmptyArray() throws Exception {
        assertEquals("Empty arrays should have a max of 0", 0, Max.sequence(new int[]{}));
    }

    @Test
    public void testNegativeArray() throws Exception {
        assertEquals("Only negative arrays should have a max of 0", 0, Max.sequence(new int[]{-5,-8,-1,-2,-3}));
    }

    @Test
    public void isArrayNegativeOrEmpty_returns_true_for_empty_array() {
        assertEquals("Empty array should return true", true, Max.isArrayNegativeOrEmpty(new int[]{}));
    }

    @Test
    public void isArrayNegativeOrEmpty_returns_true_for_array_with_only_nevatives() {
        assertEquals("Negative array should return true", true, Max.isArrayNegativeOrEmpty(new int[]{-5,-8,-1,-2,-3}));
    }

    @Test
    public void isArrayNegativeOrEmpty_returns_false_for_array_with_mixed_positive_and_negative() {
        assertEquals("Mixed array should return false", false, Max.isArrayNegativeOrEmpty(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4}));
    }

    @Test
    public void splitIntoContiguousSubArrays_5_element_array_subArray_size_of_3_returns_3_subarrays() {
        int[] arrayToSplit = {1,2,3,4,5};
        int subArraySize = 3;

        List<int[]> actualSubarrays = Max.splitIntoContiguousSubArrays(arrayToSplit, subArraySize);

        List<int[]> expectedSubarrays = new ArrayList<>();
        expectedSubarrays.add(new int[]{1,2,3});
        expectedSubarrays.add(new int[]{2,3,4});
        expectedSubarrays.add(new int[]{3,4,5});
        assertEquals(expectedSubarrays.size(), actualSubarrays.size());
    }

    @Test
    public void splitIntoContiguousSubArrays_3_element_array_subArray_size_of_3_returns_1_subarrays() {
        int[] arrayToSplit = {1,2,3};
        int subArraySize = 3;

        List<int[]> actualSubarrays = Max.splitIntoContiguousSubArrays(arrayToSplit, subArraySize);

        List<int[]> expectedSubarrays = new ArrayList<>();
        expectedSubarrays.add(new int[]{1,2,3});
        assertEquals(expectedSubarrays.size(), actualSubarrays.size());
    }

    @Test
    public void splitIntoContiguousSubArrays_3_element_array_subArray_size_of_4_returns_0_subarrays() {
        int[] arrayToSplit = {1,2,3};
        int subArraySize = 4;

        List<int[]> actualSubarrays = Max.splitIntoContiguousSubArrays(arrayToSplit, subArraySize);

        List<int[]> expectedSubarrays = new ArrayList<>();
        assertEquals(expectedSubarrays.size(), actualSubarrays.size());
    }
}
