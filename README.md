# codewars-java-katas

Just a collections of katas/solutions for Java problems from codewars.  (https://www.codewars.com)  Frankly, their in-browser editor is a chore to work with, so I prefer to create the solutions in IntelliJ and then copy-paste them into the browser.